---
title: "Lab 1."
tags: ""
---
# Linux Node

#### Step 1. Change nimda psswd

login as root

```bash
root@ubuntu:/ passwd nimda Elserg58
```

#### Step 2. Create user detimil

```bash
root@ubuntu:/ useradd -m detimil
# -m for creating home dir
root@ubuntu:/ passwd detimil #detimil
```

#### Step 3. Сonfigure time

```bash
# Check Time 
date 

# Check Timezone
timedatectl

# start the service systemd-timesync
sudo systemctl start systemd-timesyncd

# Configure /etc/systemd/timesyncd.conf
# Set time server
sudo nano /etc/systemd/timesyncd.conf

[Time]
NTP=3.europe.pool.ntp.org
FallbackNTP=ee.pool.ntp.org
#RootDistanceMaxSec=5
#PollIntervalMinSec=32
#PollIntervalMaxSec=2048

# Restart systemd-timesync to apply changes
sudo systemctl restart systemd-timesyncd

# Check status and logs
sudo systemctl status systemd-timesyncd

● systemd-timesyncd.service - Network Time Synchronization
   Loaded: loaded (/lib/systemd/system/systemd-timesyncd.service; enabled; vendor preset: enabled)
   Active: active (running) since Sun 2020-10-18 21:39:44 +12; 16min ago
     Docs: man:systemd-timesyncd.service(8)
 Main PID: 3012 (systemd-timesyn)
   Status: "Synchronized to time server 212.25.15.128:123 (3.europe.pool.ntp.org)."
    Tasks: 2 (limit: 2318)
   CGroup: /system.slice/systemd-timesyncd.service
           └─3012 /lib/systemd/systemd-timesyncd

Oct 21 16:47:43 lab69lnx systemd[1]: Starting Network Time Synchronization...
Oct 21 16:47:44 lab69lnx systemd[1]: Started Network Time Synchronization.
Oct 21 16:47:44 lab69lnx systemd-timesyncd[3012]: Synchronized to time server 51.68.198.213:213 (3.europe.pool.ntp.org).

# All logs
journalctl --unit systemd-timesyncd.service >> ../home/detimil/ics0020-labs-outcome/lab01-lnx-forced.time.sync
```

Repeat this on the mgmt machine

#### Step 4. Text logs (web servers)

```bash
# Install nginx
sudo apt install nginx

# Check opened ports (80 should be open) and own ip
netstat -ntlp
ifconfig 

# Create index2 file
cd var/www
sudo touch index2.html 

# Test if index2.html is up
curl http://$ip/index2.html

# Check logs after trying to access
cat /var/log/nginx/access.log

```

#### Step 5. Built-in logging facilities

```bash
# Using shell commands query on on labXXb and labXXwin
# all “change password” attempts
sudo cat /var/log/auth.log | grep -i 'password'
# all failed login attempts 
sudo last -f /var/log/btmp
# all successful logins 
sudo last -f var/log/wtmp
# all services stopped since last boot
sudo cat syslog| grep -e 'service'
```

Output:

```bash
4625: An account failed to log on
4624 	An account was successfully logged on 
4648 	A logon was attempted using explicit credentials
4723: An attempt was made to change an account's password
4724: An attempt was made to reset an accounts password 
4738: A user account was changed 
4771: Kerberos pre-authentication failed
```

#### Step 6. Accidental entry of password as login name

```bash
sudo cat /var/log/auth.log | grep -i 'Invalid user'
```

# Server mgmt

#### Step 1. Change nimda psswd

login as root

```bash
root@ubuntu:/ passwd nimda Elserg58
```

#### Step 2. Create user detimil

```bash
root@ubuntu:/# useradd -m detimil
# -m for creating home dir
root@ubuntu:/ passwd detimil detimil
```

#### Step 3. Create folder in detimir homedir

login as detimil

```bash
$ mkdir ics0020-labs-outcome
```

#### Step 4. Create SSH keypair and spread between users

```bash
root@ubuntu:/# ssh-keygen
#copy to detimil
cp -r root/.ssh home/detimil
#from root give permissions
sudo chown detimil:detimil home/detimil/.ssh
```

#### Step 5. Install Ansible

```bash
apt-get install python3-venv
python3 -m venv ~/ansible-venv
~/ansible-venv/bin/pip install ansible==2.9
~/ansible-venv/bin/ansible --version
```

#### Step 6. In the homedir of detimil user create folder ics0020-ansible

```bash
su - detimil
mkdir ics0020-ansible
cd ics0020-ansible
touch hosts
touch ansible.cfg
# add inventory to ansible.cfg
	[defaults]
    inventory = ./hosts
```

#### Step 7. Validate access to mgmt and lnx using ansible module ping

```bash
#in hosts:
win ansible_host=detimil@192.168.161.194 ansible_port=22 ansible_ssh_user=detimil ansible_python_interpreter=python3
lnx ansible_host=detimil@192.168.161.193 ansible_port=22 ansible_ssh_user=detimil ansible_python_interpreter=python3
```

In `ics0020-ansible` create `roles/test_connection/tasks/main.yaml`

```bash
#in main/yaml
    ---
    - name: Ansible ping module
      ping:
  
#in ics0020-ansible
nano test_ansible.yaml

#in test_ansible.yaml
    ---
    - name: Ansible connection test
      hosts: all
      roles:
        - test_connection
```

#### Step 8. Install smallstep (online CA for secure, automated X.509 and SSH certificate management)

```bash
wget https://github.com/smallstep/cli/releases/download/v0.15.2/step_linux_0.15.2_amd64.tar.gz
wget https://github.com/smallstep/certificates/releases/download/v0.15.2/step-certificates_linux_0.15.2_amd64.tar.gz

# Initialize it
step ca init -name=localCA -dns=localhost -address=127.0.0.1:443 -provisioner=root@localhost

# Run server
step-ca $(step path)/config/ca.json

# Install certificate
step certificate install root_ca.crt

# Enable acme
step ca provisioner add acme --type ACME

# Check
curl --cacert root_ca.crt https://localhost/acme/acme/directory

# Certbot and certbot plugin for nginx
apt install certbot python-pip  -y
pip install certbot-nginx
```

#### Step 9. Create SSL certificate

```bash
sudo openssl req -x509 -nodes -days 365 -newkey rsa:2048 -keyout /etc/ssl/private/nginx-selfsigned.key -out /etc/ssl/certs/nginx-selfsigned.crt

sudo openssl dhparam -out /etc/nginx/dhparam.pem 4096
```

#### Step 10. Configure nginx to use SSL

```bash
# Location of CA & SSL.key 
sudo nano /etc/nginx/snippets/self-signed.conf

# Create a configuration specifying the encryption algorithm of SSL
sudo nano /etc/nginx/snippets/ssl-params.conf

# In ssl-params.conf:
    ssl_protocols TLSv1.2;
    ssl_prefer_server_ciphers on;
    ssl_dhparam /etc/nginx/dhparam.pem;
    ssl_ciphers ECDHE-RSA-AES256-GCM-SHA512:DHE-RSA-AES256-GCM-SHA512:ECDHE-RSA-AES256-GCM-SHA384:DHE-RSA-AES256-GCM-SHA384:ECDHE-RSA-AES256-SHA384;
    ssl_ecdh_curve secp384r1; # Requires nginx >= 1.1.0
    ssl_session_timeout  10m;
    ssl_session_cache shared:SSL:10m;
    ssl_session_tickets off; # Requires nginx >= 1.5.9
    ssl_stapling on; # Requires nginx >= 1.3.7
    ssl_stapling_verify on; # Requires nginx => 1.3.7
    resolver 8.8.8.8 8.8.4.4 valid=300s;
    resolver_timeout 5s;
    # Disable strict transport security for now. You can uncomment the following
    # line if you understand the implications.
    # add_header Strict-Transport-Security "max-age=63072000; includeSubDomains; preload";
    add_header X-Frame-Options DENY;
    add_header X-Content-Type-Options nosniff;
    add_header X-XSS-Protection "1; mode=block";
    
# Configure nginx to listen to ssl server
sudo nano /etc/nginx/sites-available/your_domain

# In your_domain:
    server {
        listen 443 ssl default_server;
        listen [::]:443 ssl default_server;

        # SSL certificate
        include snippets/self-signed.conf;

        root /var/www/html; 
        index gama.gif; 

        server_name _;

        location / {
            # First attempt to serve request as file, then
            # as directory, then fall back to displaying a 404.
            try_files $uri $uri/ =404;
        }
    }

    server {
        listen 80;
        listen [::]:80;

        server_name _;

        root /var/www/html/;
        index index.html;

    }

# Finally restart nginx to apply changes
sudo nginx -t # nginx test
sudo service nginx restart
sudo ervice nginx status
```

# Windows Node

#### Step 1. Change nimda psswd

runas admin

```powershell
PS C:\Users\nimda> net user nimda Elserg58
```

#### Step 2. Create user detimil

psswd - Detimil123

```powershell
PS C:\Users\nimda> net user /add detimil <password> /homedir:C\Users\detimil
```

#### Step 3. Сonfigure time

```bash
#Check Time
Get-Date

#Check Timezone
Get-TimeZone

#Check Time Server
w32tm /query /status

#Set Timezone to UTC
tzutil /s 0

#Force Time Sync // This command requires elevated rights
W32tm /resync /force
```

#### Step 4. Text logs (web servers)

```bash
#Install IIS
Install-WindowsFeature -name Web-Server -IncludeManagementTools
```

Then add index2.html with the following contents:

```html
<!doctype html>
<html>
  <head>
    <title>title</title>
  </head>
  <body>
    <p>Hello!</p>
  </body>
</html>
```

via command `Move-Item -Path C:\Users\nimda\Desktop\index2.html -Destination C:\inetpub\wwwroot\index2.html`

Check log files in `C:\inetpub\logs\LogFiles\W3SVC1\*.log`
