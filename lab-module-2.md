---
title: "Lab Module 2"
tags: ""
---
# Linux Node

#### Step 1. Forward to relay

For forwarding messages to mgmt relay in file `/etc/rsyslog.d/50-default.conf`:

```bash
*.* {
        action(type="omfwd" target="192.168.180.3" port="514" protocol="tcp")
}
```

For showing messages on mgmt:

1.  generate error message with `logger -p <any_facility>.err "<your_message>"` command

2.  To check log messages use either `tail -f /var/log/syslog` or `journalctl | grep "<your_message>" /var/log/syslog`

#### Step 2. UDP reception

In `/etc/rsyslog.conf` for UDP listener:

```bash
    module(load="imudp")
    input(type="imudp" port="514")
```

Tested with `netstat` and `nc -vzu <host> 514`

#### Step 3. (after configuring win) Forward Windows data only

In `/etc/rsyslog.conf`

```bash
# New ruleset for UDP forwarding
ruleset(name="win_data") {
 action(type="omfwd" Target="<mgmt_IP>" Port="514"
Protocol="tcp")
}

# Bind UDP listener to win_data
module(load="imudp")
input(type="imudp" port="514" ruleset="win_data")

# Change /etc/rsyslog.d/50-default.conf accordingly
call win_data
```

#### Step 3. Read data from .txt

1.  Add module `imfile` to `/etc/rsyslog.conf`

2.  Configure listener

```bash
input(type="imfile" File="/var/log/<file>" Tag="<tag>" ruleset="win_data")
```

#### Step 4. Apply local preprocessing before relaying (relay only some messages)

Add to `/etc/rsyslog.d/50-
default.conf`

```bash
if ($msg contains "junk") then {
 stop
}
```

# Server mgmt

#### Step 1. Create a simple inbound TCP and UDP listeners

```bash
nano /etc/rsyslog.conf

# In rsyslog.conf
    #for tcp
    module(load="imtcp")
    input(type="imtcp" port="514")

    #for udp
    module(load="imudp")
    input(type="imudp" port="514")
```

#### Step 2. Test connectivity

```bash
#tcp
nc -vz localhost 514 
#udp
nc -vzu localhost 514
#listeners on host
netstat -lnptu
```

#### Step 3. Convert .conf files to a new style

##### /etc/rsyslog.d/21-cloudinit.conf

```bash
# OLD
:syslogtag, isequal, "[CLOUDINIT]" /var/log/cloud-init.log
& stop

# NEW
if $syslogtag == "[CLOUDINIT]" then {
  action(type="omfile" file="/var/log/clound-init.log")
  stop
}
```

##### /etc/rsyslog.d/20-ufw.conf

```bash
# OLD
:msg,contains,"[UFW " /var/log/ufw.log

# NEW
if $msg contains "[UFW " then {
  action(type="omfile" file="/var/log/ufw.log")
}
```

##### /etc/rsyslog.d/50-default.conf

```bash
# OLD
auth,authpriv.*            /var/log/auth.log
*.*;auth,authpriv.none     -/var/log/syslog
kern.*                     -/var/log/kern.log
mail.*                     -/var/log/mail.log

mail.err                   /var/log/mail.err

*.emerg                    :omusrmsg:*

# NEW
auth,authpriv.* {
 action(type="omfile" file="/var/log/auth.log")
}

*.*;auth,authpriv.none {
 action(type="omfile" file="/var/log/syslog")
}

kern.* {
 action(type="omfile" file="/var/log/kern.log")
}

mail.* {
 action(type="omfile" file="/var/log/mail.log")
}

mail.err {
 action(type="omfile" file="/var/log/mail.err")
}
# omfile saves log information into specified file

*.emerg {
action(type="omusrmsg" users="*")
} 
# omusrmsg messages system users
```

#### Step 4. (After configuring win) Check logs

```bash
tail -f /var/log/syslog
```

# Windows Node

#### Step 1. Send events

1.  Install `rsyslog` [from here](https://www.rsyslog.com/windows-agent/windows-agent-download/)

2.  Guides for configuration: [here](https://rainer.gerhards.net/2019/10/rsyslog-integrating-windows-event-log-via-udp.html), [here](https://www.rsyslog.com/how-to-setup-the-forward-via-syslog-action/), and [here](https://www.rsyslog.com/how-to-setup-eventlogmonitor-v2-service/)
